#ifndef lois_aleatoires_h
#define lois_aleatoires_h


#include "mt19937ar.h"


/**
 * @brief Renvoie un entier aléatoire suivant une loi uniforme
 * dans [|a, b|]
 * @return un entier dans [|a, b|]
 */
int uniformeEntier(int a, int b);


/**
 * @brief Renvoie un entier suivant une loi normale
 * @param mu : moyenne
 * @param sigma : écart-type
 * @return un entier suivant une loi normale
 */
double gausienneEntier(double mu, double sigma);

#endif