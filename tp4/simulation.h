/**
 * Fichier d'en-tête qui contient la déclarations des fonctions de simulation.c
 * et la définition de tous les paramètres que la simulation utilise.
 */

#ifndef code_h_guardian
#define code_h_guardian

#include <stdlib.h>
#include <stdio.h>
//#include <math.h>
//#include <time.h>

#include "mt19937ar.h"
#include "lois_aleatoires.h"

/* ---- Définition des paramètres de simulation ---- */
//Mode de simulation
#define SIMULATION_AVEC_RESSOURCES // Pour simuler sans ressources mettre la ligne en commentaire
#define NB_RESSOURCES 1000         // Utilisé seulement si SIMULATION_AVEC_RESSOURCES est défini

// Paramètre généraux
#define NB_MAXIMAL_LAPINS 10000
#define NB_INITIAL_LAPINS 80
#define NB_MOIS_A_SIMULER 240 // 20 ans
#define NB_REPLICATIONS   100

// Constantes qui associent le début d'une période de la vie d'un lapin à un 
// nombre de mois
#define ENFANT 0
#define ADULTE 4
#define VIEUX  24 // 2 ans

// Taux de survie d'un lapin à chaque mois en fonction de son âge
#define TAUX_SURVIE_ENFANT 0.707
/*
Pour le taux de survie des lapins adultes :
-  5 % de vieux (taux de base) : 0.8609
- 10 % de vieux : 0.8912
- 12 % de vieux : 0.8994
- 15 % de vieux : 0.9095
- 25 % de vieux : 0.9330
*/
#define TAUX_SURVIE_ADULTE 0.9330
#define REDUCTION_TAUX_SURVIE_PAR_MOIS 0.05 // un lapin vieux voit son taux de survie baisser chaque mois

#define TAUX_MALES  0.5     // On en déduit le taux de femelles

// Le nombre de portées par an d'une lapine suit une loi uniforme dans
// l'intervalle spécifié ci-desssous
#define NB_PORTEES_MINIMAL 3
#define NB_PORTEES_MAXIMAL 5

// Le nombre de lapereaux par portée suit une loi normale de moyenne 
// et d'écart type spécifiés ci-dessous
#define MOYENNE_LAPERAUX_PAR_PORTEE 7.5
#define ECART_TYPE_LAPERAUX_PAR_PORTEE 3
#define NB_LAPEREAUX_MAX_PAR_PORTEE 12  // Si le nombre tiré n'est pas dans l'intervalle, on
#define NB_LAPEREAUX_MIN_PAR_PORTEE 3   // choisit le nombre limite définit ici suivant le cas


/* ---- Définition de constantes ---- */
// Booléens
#define TRUE  1
#define FALSE 0

// Sexe des lapins
#define MALE    0
#define FEMELLE 1

// Temps
#define ANNEE 12 // Une année fait 12 mois 



typedef struct
{
    int age;
    double taux_survie;
    int sexe;

    // Ces paramètres ne concernent que les lapines
    int portees[NB_PORTEES_MAXIMAL];
    int num_portee;

} Lapin;


typedef struct
{
    int nb_morts;
    int nb_naissances;
    int nb_vivants;

    int nb_enfants;
    int nb_adultes;
    int nb_vieux;
    int nb_males;
    int nb_femelles;

} StatistiquesSimulationParMois;


typedef struct
{
    Lapin population[NB_MAXIMAL_LAPINS];
    int nb_lapins_vivants;

    int numero_mois;

    StatistiquesSimulationParMois stats[NB_MOIS_A_SIMULER];

} Simulation;


/**
 * @brief Choisit un genre pour un lapin selon le taux de mâles
 * spécifié par TAUX_MALES
 * @return MALE | FEMELLE
 */
int choisirSexeLapin();


/**
 * @brief Choisit si une femelle a une portée à un mois donné
 * @param l : une lapine
 * @return TRUE | FALSE
 */
int aUnePortee(Lapin * l);


/**
 * @brief Choisit le nombre de portées qu'aura une lapine au cours de
 * l'année ainsi que ses mois de gestation
 * @param l : une lapine
 */
void choisirNombreDePorteesPourAnnee(Lapin * l);


/**
 * @brief Donne les attributs d'un nouveau né à un lapin
 * @param l : le lapin à initialiser
 */
void faireNaitreLapin(Lapin * l);


/**
 * @brief Indique si un lapin est mort ou pas
 * @param l : le lapin
 * @return TRUE | FALSE
 */
int estMort(Lapin * l);


/**
 * @brief Tue un lapin
 * @param l : le lapin à tuer
 */
void tuerLapin(Lapin * l);


/**
 * @brief Choisit un nombre d'enfants si une femelle a une portée
 * @return un nombre d'enfants
 */
int choisirNbEnfants();


/**
 * @brief Détermine si un lapin survit pour un mois donné en prenant en
 * compte le nombre de ressources disponibles et du nombre de lapins vivants
 * @param taux_de_survie : taux de survie du lapin
 * @param[in] s : SDD de la simulation
 * @return TRUE | FALSE
 */
int lapinSurvitAvecRessource(double taux_de_survie, Simulation * s);


/**
 * @brief Détermine si un lapin survit pour un mois donné
 * @param taux_de_survie : taux de survie du lapin
 * @return TRUE | FALSE
 */
int lapinSurvit(double taux_de_survie);


/**
 * @brief Ajoute de nouveaux lapins dans la population
 * 
 * Remarque : la fonction fait en sorte que l'ajout de nouveaux lapins ne
 * dépasse pas la taille maximale du tableau.
 * 
 * @param[in, out] s : SDD de la simlation
 * @param nb_naissances : nombre de lapins à faire naître
 */
void creerNouveauxNes(Simulation * s, int nb_naissances);


/**
 * @brief Copie le contenu d'un lapin dans un autre lapin
 * @param[in] src : lapin source
 * @param[out] dest : lapin destination
 */
void copierLapin(Lapin * src, Lapin * dest);


/**
 * @brief Echange le contenu de deux lapins
 * @param a : lapin 1
 * @param b : lapin 2
 */
void echangerLapins(Lapin * a, Lapin * b);


/**
 * @brief Enlève tous les lapins morts de la population
 * @param[in, out] s : SDD de la simulation
 * @param nb_tues : nombre de lapins mort dans la population
 */
void supprimerLapinsMorts(Simulation * s, int nb_tues);


/**
 * @brief Initialise la population
 * @param[out] : SDD de la simulation
 */
void initialiser_population(Simulation * s);


/**
 * @brief Stocke les données d'évolution de la population pour le mois
 * qui vient de s'écouler
 * @param[in, out] s : SDD de la simulation
 * @param nb_naissances : nombre de naissances au cours du mois
 * @param nb_morts : nombr de morts au cours du mois
 */
void collecterStatsDuMois(Simulation * s, int nb_naissances, int nb_morts);


/**
 * @brief Fait avancer la simulation d'un pas de temps qui correspond
 * à un mois
 * @param[in ,out] s : SDD de la simulation
 */
void simulerUnMois(Simulation * s);


/**
 * @brief Effectue une simulation
 * @param[in, out] s : SDD de la simulation
 */
void simulation(Simulation * s);


/**
 * @brief Stoke le résultat d'une simulation dans un fichier
 * @param[in] s : SDD de la simulation
 * @param num_replication : numéro de la réplication de la simulation
 */
void ecrireResultatSimulationDansFichier(Simulation * s, int num_replication);


/**
 * @brief Effectue un certain nombre de simulations
 */
void replicationsSimulation();


#endif