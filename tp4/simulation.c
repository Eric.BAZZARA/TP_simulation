/* Ce fichier contient les fonctions qui permettent de simuler 
 * l'évolution d'une population de lapins
 */
#include "simulation.h"


/**
 * @brief Choisit un genre pour un lapin selon le taux de mâles
 * spécifié par TAUX_MALES
 * @return MALE | FEMELLE
 */
int choisirSexeLapin()
{
    int res = MALE;

    if (genrand_real1() > TAUX_MALES) res = FEMELLE;

    return res;
}


/**
 * @brief Choisit si une femelle a une portée à un mois donné
 * @param l : une lapine
 * @return TRUE | FALSE
 */
int aUnePortee(Lapin * l)
{   
    int res = FALSE;

    if (l->portees[l->num_portee] == l->age)
    {
        res = TRUE;
        l->num_portee++;

        // Pour ne pas sortir du tableau au prochain appel de la fonction
        if (l->num_portee == NB_PORTEES_MAXIMAL) l->num_portee--; 
    }

    return res;
}


/**
 * @brief Choisit le nombre de portées qu'aura une lapine au cours de
 * l'année ainsi que ses mois de gestation
 * @param l : une lapine
 */
void choisirNombreDePorteesPourAnnee(Lapin * l)
{
    int mois[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    int index_mois_tire, mois_tire;

    int nb_portees = uniformeEntier(NB_PORTEES_MINIMAL, NB_PORTEES_MAXIMAL);
    int i;

    for (i = 0; i < nb_portees; i++)
    {   
        index_mois_tire       = uniformeEntier(0, 11 - i);
        mois_tire             = mois[index_mois_tire];
        mois[index_mois_tire] = mois[11 - i];


        l->portees[i] = mois_tire + l->age;
    }

    for (; i < NB_PORTEES_MAXIMAL; i++) l->portees[i] = 0;

    l->num_portee = 0;
}


/**
 * @brief Donne les attributs d'un nouveau né à un lapin
 * @param l : le lapin à initialiser
 */
void faireNaitreLapin(Lapin * l)
{
    l->age         = ENFANT;
    l->taux_survie = TAUX_SURVIE_ENFANT;
    l->sexe        = choisirSexeLapin();
}


/**
 * @brief Indique si un lapin est mort ou pas
 * @param l : le lapin
 * @return TRUE | FALSE
 */
int estMort(Lapin * l)
{
    return l->taux_survie == 0.0;
}


/**
 * @brief Tue un lapin
 * @param l : le lapin à tuer
 */
void tuerLapin(Lapin * l)
{
    l->taux_survie = 0.0;
}


/**
 * @brief Choisit un nombre d'enfants si une femelle a une portée
 * @return un nombre d'enfants
 */
int choisirNbEnfants()
{   
    int res = gausienneEntier(MOYENNE_LAPERAUX_PAR_PORTEE, ECART_TYPE_LAPERAUX_PAR_PORTEE);

    if      (res < NB_LAPEREAUX_MIN_PAR_PORTEE) res = NB_LAPEREAUX_MIN_PAR_PORTEE;
    else if (res > NB_LAPEREAUX_MAX_PAR_PORTEE) res = NB_LAPEREAUX_MAX_PAR_PORTEE;
    
    return res;
}


/**
 * @brief Détermine si un lapin survit pour un mois donné en prenant en
 * compte le nombre de ressources disponibles et du nombre de lapins vivants
 * @param taux_de_survie : taux de survie du lapin
 * @param[in] s : SDD de la simulation
 * @return TRUE | FALSE
 */
int lapinSurvitAvecRessource(double taux_de_survie, Simulation * s)
{
    int res = TRUE;
    double nb_lapins = (double) s->nb_lapins_vivants;

    if (nb_lapins < NB_RESSOURCES){
        taux_de_survie = (nb_lapins * (taux_de_survie - 1.0) / NB_RESSOURCES) + 1.0;
    }
    else if (nb_lapins < 2 * NB_RESSOURCES){
        taux_de_survie = (-nb_lapins * taux_de_survie / NB_RESSOURCES) + (2 * taux_de_survie);
    }
    else{
        taux_de_survie = 0.0;
    }
        

    if (genrand_real1() > taux_de_survie) res = FALSE;

    return res;
}


/**
 * @brief Détermine si un lapin survit pour un mois donné
 * @param taux_de_survie : taux de survie du lapin
 * @return TRUE | FALSE
 */
int lapinSurvit(double taux_de_survie)
{
    int res = TRUE;

    if (genrand_real1() > taux_de_survie) res = FALSE;

    return res;
}


/**
 * @brief Ajoute de nouveaux lapins dans la population
 * 
 * Remarque : la fonction fait en sorte que l'ajout de nouveaux lapins ne
 * dépasse pas la taille maximale du tableau.
 * 
 * @param[in, out] s : SDD de la simlation
 * @param nb_naissances : nombre de lapins à faire naître
 */
void creerNouveauxNes(Simulation * s, int nb_naissances)
{
    int nouveaux = 0;

    while (s->nb_lapins_vivants < NB_MAXIMAL_LAPINS && nouveaux < nb_naissances)
    {
        faireNaitreLapin(s->population + s->nb_lapins_vivants);
        s->nb_lapins_vivants++;
        nouveaux++;
    }
}


/**
 * @brief Copie le contenu d'un lapin dans un autre lapin
 * @param[in] src : lapin source
 * @param[out] dest : lapin destination
 */
void copierLapin(Lapin * src, Lapin * dest)
{
    dest->age         = src->age;
    dest->num_portee  = src->num_portee;
    dest->sexe        = src->sexe;
    dest->taux_survie = src->taux_survie;

    for (int i = 0; i < NB_PORTEES_MAXIMAL; i++) 
    {
        dest->portees[i] = src->portees[i];
    }
}


/**
 * @brief Echange le contenu de deux lapins
 * @param a : lapin 1
 * @param b : lapin 2
 */
void echangerLapins(Lapin * a, Lapin * b)
{
    Lapin temp;

    copierLapin(a, &temp);
    copierLapin(b, a);
    copierLapin(b, &temp);
}


/**
 * @brief Enlève tous les lapins morts de la population
 * @param[in, out] s : SDD de la simulation
 * @param nb_tues : nombre de lapins mort dans la population
 */
void supprimerLapinsMorts(Simulation * s, int nb_tues)
{
    int index_mort   = 0,                    // index du premier lapin mort trouvé depuis le début de la liste
        i            = 0,                    
        index_vivant = s->nb_lapins_vivants; // index du premier lapin vivant trouvé depuis la fin de liste

    
    while (i < nb_tues && index_mort < index_vivant)
    {
        while (index_mort < index_vivant && !estMort(s->population + index_mort))   index_mort++;
        while (index_mort < index_vivant &&  estMort(s->population + index_vivant)) index_vivant--;

        if (index_mort < index_vivant)
        {
            echangerLapins(s->population + index_mort, s->population + index_vivant);
        }

        i++; 
    }

    s->nb_lapins_vivants -= nb_tues;
}


/**
 * @brief Initialise la population
 * @param[out] : SDD de la simulation
 */
void initialiser_population(Simulation * s)
{   
    int nb_lapins_initial = NB_INITIAL_LAPINS;

    for (int i = 0; i < nb_lapins_initial; i++)
    {   
        s->population[i].age         = ADULTE;
        s->population[i].taux_survie = TAUX_SURVIE_ADULTE;
        s->population[i].sexe        = choisirSexeLapin();

        if (s->population[i].sexe == FEMELLE){
            choisirNombreDePorteesPourAnnee(s->population + i);
        }
    }

    s->nb_lapins_vivants = nb_lapins_initial;
    s->numero_mois       = 0;
}


/**
 * @brief Stocke les données d'évolution de la population pour le mois
 * qui vient de s'écouler
 * @param[in, out] s : SDD de la simulation
 * @param nb_naissances : nombre de naissances au cours du mois
 * @param nb_morts : nombr de morts au cours du mois
 */
void collecterStatsDuMois(Simulation * s, int nb_naissances, int nb_morts)
{
    StatistiquesSimulationParMois * stat = s->stats + s->numero_mois;
    Lapin * l;

    stat->nb_vivants    = s->nb_lapins_vivants;
    stat->nb_naissances = nb_naissances;
    stat->nb_morts      = nb_morts;

    stat->nb_enfants = 0;
    stat->nb_adultes = 0;
    stat->nb_vieux   = 0;

    stat->nb_femelles = 0;
    stat->nb_males    = 0;
    

    for (int i = 0; i < s->nb_lapins_vivants; i++)
    {   
        l = s->population + i;

        if (l->sexe == MALE) stat->nb_males++;
        else                 stat->nb_femelles++;

        if      (l->age < ADULTE) stat->nb_enfants++;
        else if (l->age < VIEUX)  stat->nb_adultes++;
        else                      stat->nb_vieux++;
    }
}


/**
 * @brief Fait avancer la simulation d'un pas de temps qui correspond
 * à un mois
 * @param[in ,out] s : SDD de la simulation
 */
void simulerUnMois(Simulation * s)
{
    Lapin * lapin;
    int nb_tues       = 0,
        nb_naissances = 0;

    // Traitement des lapins vivants
    for (int i = 0; i < s->nb_lapins_vivants; i++)
    {   
        lapin = s->population + i;

        // Mise à jour de l'âge
        lapin->age++;

        // Mise à jour du taux de survie
        if      (lapin->age == ADULTE) lapin->taux_survie  = TAUX_SURVIE_ADULTE;
        else if (lapin->age >= VIEUX)  lapin->taux_survie -= REDUCTION_TAUX_SURVIE_PAR_MOIS;

        // On regarde si le lapin survit ou pas
        #ifdef SIMULATION_AVEC_RESSOURCES
        if (lapinSurvitAvecRessource(lapin->taux_survie, s))
        #else
        if (lapinSurvit(lapin->taux_survie))
        #endif
        {   
            // Reproduction
            if (lapin->sexe == FEMELLE && lapin->age >= ADULTE)
            {   
                // A chaque date anniversaire du passage à l'âge adulte, on choisit un calendrier
                // de maternité pour l'année qui va s'écouler
                if (lapin->age % ANNEE == ADULTE % ANNEE) choisirNombreDePorteesPourAnnee(lapin);

                if (aUnePortee(lapin)) nb_naissances += choisirNbEnfants();
            }
        }
        else
        {
            tuerLapin(lapin);
            nb_tues++;
        }
    }

    // Suppression des lapins morts
    supprimerLapinsMorts(s, nb_tues);

    // Création des nouveaux nés
    creerNouveauxNes(s, nb_naissances);

    // Stockage des stats
    collecterStatsDuMois(s, nb_naissances, nb_tues);

    //Augmentation du pas de temps
    s->numero_mois++;
}


/**
 * @brief Effectue une simulation
 * @param[in, out] s : SDD de la simulation
 */
void simulation(Simulation * s)
{   
    initialiser_population(s);

    while (s->numero_mois < NB_MOIS_A_SIMULER && s->nb_lapins_vivants < NB_MAXIMAL_LAPINS)
    {   
        simulerUnMois(s);
    } 
}


/**
 * @brief Stoke le résultat d'une simulation dans un fichier
 * @param[in] s : SDD de la simulation
 * @param num_replication : numéro de la réplication de la simulation
 */
void ecrireResultatSimulationDansFichier(Simulation * s, int num_replication)
{
    char patron_nom_fichier[] = "replications/replication_%d.csv";
    char nom_fichier[100];

    snprintf(nom_fichier, 100, patron_nom_fichier, num_replication);

    FILE * fichier = fopen(nom_fichier, "w");
    if (fichier == NULL) 
    {
        printf("Erreur : Echec de l'ouverture du fichier\n");
        exit(EXIT_FAILURE);
    }

    fprintf(fichier, "Mois;Vivants;Enfants;Adultes;Vieux\n");
    int i = 0;
    while (i < NB_MOIS_A_SIMULER)
    {
        fprintf(fichier, "%d; %d; %d; %d; %d\n",
            i,
            s->stats[i].nb_vivants,
            s->stats[i].nb_enfants,
            s->stats[i].nb_adultes,
            s->stats[i].nb_vieux);

        if (s->stats[i].nb_vivants == NB_MAXIMAL_LAPINS){
            // On sort de la boucle car la simulation c'est arêté à ce moment là
            i = NB_MOIS_A_SIMULER; 
        }
        else i++;
    }

    fclose(fichier);
}


/**
 * @brief Effectue un certain nombre de simulations
 */
void replicationsSimulation()
{   
    Simulation s;

    for (int i = 0; i < NB_REPLICATIONS; i++)
    {   
        simulation(&s);
        ecrireResultatSimulationDansFichier(&s, i);
    } 
}


int main()
{   
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    printf("Début des simulations\n");
    replicationsSimulation();
    printf("Fin des simulations\n");

    return EXIT_SUCCESS;
}