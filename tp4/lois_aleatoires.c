/* Fichier qui contient les fonctions permettant de tirer des nombres aléatoires */

#include "lois_aleatoires.h"


/**
 * @brief Renvoie un entier aléatoire suivant une loi uniforme
 * dans [|a, b|]
 * @return un entier dans [|a, b|]
 */
int uniformeEntier(int a, int b)
{   
    return ((int) genrand_int31()) % (b - a + 1) + a;
}


/**
 * @brief Renvoie un entier suivant une loi normale
 * @param mu : moyenne
 * @param sigma : écart-type
 * @return un entier suivant une loi normale
 */
double gausienneEntier(double mu, double sigma)
{   
    double x0, r2;

    x0 = sqrt( -2 * log(genrand_real3()) );
    r2 = genrand_real3();

    return (int) (cos(2 * 3.1415 * r2) * x0 * sigma + mu);
}