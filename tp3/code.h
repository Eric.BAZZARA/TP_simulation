#ifndef code_h_guardian
#define code_h_guardian

#include <stdio.h>
#include <math.h>
#include <time.h>

#include "mt19937ar.h"

#define MILLE       1000
#define UN_MILLION  1000000
#define UN_MILLIARD 1000000000

#ifndef M_PI
#define M_PI // c'est juste pour que mon IDE arête d'afficher une erreur
#endif


/**
 * @brief Calcule une valeur approchée de pi
 * 
 * La méthode consiste à tirer un point dans [0, 1]x[0, 1]
 * puis à tester s'il appartient au cercle unité.
 * 
 * @param nb_tirages : nombre de tirages à effectuer
 * @return une estimation de pi
 */
double simPi(long nb_tirages);


/**
 * @brief Lance simPi et affiche le résultat trouvé
 * @param nb_tirages : nombre de tirages à effectuer
 */
void testerSimpi(long nb_tirages);


/**
 * @brief Effectue plusieurs appels à la fonction simPi afin 
 * d'avoir un résultat statistiquement plus fiable
 * @param nb_replications : nombre de réplications de l'expérience
 * à effectuer
 * @param nb_tirages : nombre de tirages à faire dans une expérience
 */
void replicationsSimPi(int nb_replications, long nb_tirages);


/**
 * @brief Effectue plusieurs appels à la fonction simPi afin 
 * d'avoir un résultat statistiquement plus fiable et de pouvoir
 * aussi calculer un intervalle de confiance
 * @param nb_replications : nombre de réplications de l'expérience
 * à effectuer
 * @param nb_tirages : nombre de tirages à faire dans une expérience
 */
void replicationsSimPiAvecErreur(int nb_replications, long nb_tirages);


/**
 * @brief Calcule une variance estimée sans biais
 * @param somme_carre : somme des valeurs au carré des résultats 
 * d'experiences
 * @param moyenne : moyenne des valeurs obtenues
 * @param nb_replications : nombre de valeurs obtenues
 * @return la variance estimée
 */
double calculerVariance(double somme_carre, double moyenne, int nb_replications);


/**
 * Renvoie le coefficient de Student pour obtenir un intervalle
 * de confiance à 99%
 * @param n : nombre de réplications de l'expérience
 */
double coeffStudent99PourcentDeConfiance(int n);


/**
 * @brief Effectue une troncature de la moyenne et du rayon en
 * fonction du premier chiffre significatif du rayon
 * 
 * Exemple : on donne m = 1.2358496 et r = 0.002658745
 * Le premier chiffre significatif de r est sur sa troisième décimale.
 * On veut renvoyer m' = 1.235 et r' = 0.003 (on arrondi le rayon
 * à la décimale supérieure)
 * 
 * @param moyenne[in, out] : moyenne à arrondir
 * @param rayon[in, out] : rayon à arrondir
 */
void troncature(double * moyenne, double * rayon);

#endif