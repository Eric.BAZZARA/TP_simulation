/**
 * Fichier du TP3 de simulation
 * 
 * Fonctionalités
 */

#include "code.h"

/**
 * @brief Calcule une valeur approchée de pi
 * 
 * La méthode consiste à tirer un point dans [0, 1]x[0, 1]
 * puis à tester s'il appartient au cercle unité. Le rapport
 * du nombre de point appartenant au cercle sur le nombre de 
 * point total converge vers pi/4.
 * 
 * @param nb_tirages : nombre de tirages à effectuer
 * @return une estimation de pi
 */
double simPi(long nb_tirages)
{
    long i,
         nb_points_dans_cercles = 0;
    double x, y;

    for (i = 0; i < nb_tirages; i++)
    {
        x = genrand_real1();
        y = genrand_real1();

        if (x * x + y * y < 1.0) nb_points_dans_cercles++;
    }

    return 4.0 * (double) nb_points_dans_cercles / (double) nb_tirages;
}


/**
 * @brief Lance simPi et affiche le résultat trouvé
 * @param nb_tirages : nombre de tirages à effectuer
 */
void testerSimpi(long nb_tirages)
{
    double pi = simPi(nb_tirages);

    printf("Valeur de pi calculée pour %ld essais : %lf\n", nb_tirages, pi);
    printf("Différence avec Pi : %lf\n\n", fabs(M_PI - pi));
}


/**
 * @brief Effectue plusieurs appels à la fonction simPi afin 
 * d'avoir un résultat statistiquement plus fiable
 * @param nb_replications : nombre de réplications de l'expérience
 * à effectuer
 * @param nb_tirages : nombre de tirages à faire dans une expérience
 */
void replicationsSimPi(int nb_replications, long nb_tirages)
{
    int i;
    double somme = 0, valeur_pi;

    for (i = 0; i < nb_replications; i++)
    {   
        valeur_pi = simPi(nb_tirages);
        somme += valeur_pi;
    }

    valeur_pi = somme / (double) nb_replications;
    printf("%d réplications de SimPi avec ",  nb_replications);
    printf("%ld lancers par réplication.\n", nb_tirages);
    printf("Valeur de pi trouvée : %lf\n", valeur_pi);
    printf("Différence avec Pi : %lf\n\n", fabs(M_PI - valeur_pi));
}



/**
 * @brief Effectue plusieurs appels à la fonction simPi afin 
 * d'avoir un résultat statistiquement plus fiable et de pouvoir
 * aussi calculer un intervalle de confiance à 99%
 * @param nb_replications : nombre de réplications de l'expérience
 * à effectuer
 * @param nb_tirages : nombre de tirages à faire dans une expérience
 */
void replicationsSimPiAvecErreur(int nb_replications, long nb_tirages)
{
    double 
        moyenne, variance, rayon, pi,
        coeff_student,
        d_nb_replications = (double) nb_replications,
        somme             = 0.0,
        somme_carre       = 0.0;

    // Expériences
    for (int i = 0; i < nb_replications; i++)
    {   
        pi           = simPi(nb_tirages);
        somme       += pi;
        somme_carre += pi * pi;
    }

    // Calcul de l'intervalle de confiance
    moyenne  = somme / nb_replications;
    variance = calculerVariance(somme_carre, moyenne, nb_replications);

    coeff_student = coeffStudent99PourcentDeConfiance(nb_replications);
    rayon         = coeff_student * sqrt(variance / d_nb_replications);

    troncature(&moyenne, &rayon);

    // Affichage du résultat
    printf("%d réplications de SimPi avec ", nb_replications);
    printf("%ld lancers par réplication.\n", nb_tirages);
    printf("Moyenne obtenue : %lf, ", moyenne);
    printf("rayon d'erreur : %lf\n", rayon);
    printf("Intervalle de confiance à 99%% : [%lf, %lf]\n\n",
           moyenne - rayon, moyenne + rayon);
}


/**
 * Renvoie le coefficient de Student pour obtenir un intervalle
 * de confiance à 99%
 * @param n : nombre de réplications de l'expérience
 */
double coeffStudent99PourcentDeConfiance(int n)
{   
    // Les valeurs son tirées de  
    // https://fr.wikipedia.org/wiki/Loi_de_Student
    double valeurs[] = {
        63.66, 9.925, 5.841, 4.604, 4.032,
        3.707, 3.499, 3.355, 3.250, 3.169,
        3.106, 3.055, 3.012, 2.977, 2.947,
        2.921, 2.898, 2.878, 2.861, 2.845,
        2.831, 2.819, 2.807, 2.797, 2.787,
        2.779, 2.771, 2.763, 2.756, 2.750,
    };
    double res;

    if      (n <  2) res = 1e10;
    else if (n < 31) res = valeurs[n - 2];
    else if (n < 41) res = 2.704;
    else if (n < 51) res = 2.678;
    else if (n < 61) res = 2.660;
    else if (n < 81) res = 2.639;
    else             res = 2.576;

    return res;
}


/**
 * @brief Effectue une troncature de la moyenne et du rayon en
 * fonction du premier chiffre significatif du rayon
 * 
 * Exemple : on donne m = 1.2358496 et r = 0.002658745
 * Le premier chiffre significatif de r est sur sa troisième décimale.
 * On veut renvoyer m' = 1.235 et r' = 0.003 (on arrondi le rayon
 * à la décimale supérieure)
 * 
 * @param moyenne[in, out] : moyenne à arrondir
 * @param rayon[in, out] : rayon à arrondir
 */
void troncature(double * moyenne, double * rayon)
{
    double precision     = 1.0;
    const double epsilon = 1e-15;

    // Condition qui sert à éviter d'avoir une boucle infinie
    // si le rayon est trop petit
    if (*rayon > epsilon)
    {
        while (floor(*rayon * precision) == 0.0)  precision *= 10.0;

        *rayon   = floor(*rayon   * precision + 1.0) / precision;
        *moyenne = floor(*moyenne * precision      ) / precision;
    }
}


/**
 * @brief Calcule une variance estimée sans biais
 * @param somme_carre : somme des valeurs au carré des résultats 
 * d'experiences
 * @param moyenne : moyenne des valeurs obtenues
 * @param nb_replications : nombre de valeurs obtenues
 * @return la variance estimée
 */
double calculerVariance(double somme_carre, double moyenne, int nb_replications)
{
    double r = (double) nb_replications;

    return (somme_carre - (r * moyenne * moyenne)) / (r - 1.0);
}


int main()
{
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);

    time_t start_time, end_time;
    time(&start_time);

    /*---- Question 1 ----*/
    printf("\n---- Question 1 ----\n");

    testerSimpi(MILLE);
    testerSimpi(UN_MILLION);
    //testerSimpi(UN_MILLIARD);

    /*---- Question 2 ----*/
    printf("\n---- Question 2 ----\n");

    replicationsSimPi(30, MILLE);
    replicationsSimPi(30, UN_MILLION);
    //replicationsSimPi(30, UN_MILLIARD);

    /*---- Question 3 ----*/
    printf("\n---- Question 3 ----\n");
    
    replicationsSimPiAvecErreur(30, MILLE);
    replicationsSimPiAvecErreur(30, UN_MILLION);
    //replicationsSimPiAvecErreur(30, UN_MILLIARD);

    time(&end_time);
    printf("Temps ecoulé : %lds\n", end_time - start_time);
}
